package decorator;

public interface IAlbumPhoto {
	
	public enum FormatType {
		LETTER_US,
		A5,
		LEGAL;
	}
	
	public enum OrientationType {
		PORTRAIT,
		LANDSCAPE;
	}
	
	public enum ColorType {
		GRAY,
		BLUE,
		RED,
		BLACK;
	}
	
	public enum CoverType {
		SMOOTH_CARTON,
	}
	
	float calculateCost();
	void setCost(float newCost);
}
