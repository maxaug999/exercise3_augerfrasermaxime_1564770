package decorator;

public class AAlbum implements IAlbumPhoto{
	public static final int ANY_DEFAULT_PAGES = 24;
	public static final float ANY_DEFAULT_COST = 19.99f;
	
	private int page = ANY_DEFAULT_PAGES;
	private float cost = ANY_DEFAULT_COST;
	private CoverType coverType = CoverType.SMOOTH_CARTON;
	private FormatType formatType = FormatType.LETTER_US;
	private OrientationType orientationType = OrientationType.PORTRAIT;
	private ColorType colorType;
	
	public AAlbum(ColorType colorType, float defaultPrice, FormatType defaultFormat, OrientationType defaultOrientation) {
		this.colorType = colorType;
		this.cost = defaultPrice;
		this.formatType =  defaultFormat;
		this.orientationType = defaultOrientation;
	}
	
	@Override
	public float calculateCost() {
		return this.cost;
	}
	
	public ColorType getColorType() {
		return this.colorType;
	}

	@Override
	public void setCost(float newCost) {
		this.cost = newCost;
	}
}
