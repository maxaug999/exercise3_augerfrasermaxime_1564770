package decorator;

public abstract class AlbumDecorator implements IAlbumPhoto{
	private IAlbumPhoto myComponentToDecore;

	public AlbumDecorator(IAlbumPhoto myComponentToDecore) {
		this.myComponentToDecore = myComponentToDecore;
	}

	public float calculateCost() {
		return this.getCost() + this.myComponentToDecore.calculateCost();
	}
	
	public abstract float getCost();
	public abstract void setCost(float newCost);
}
