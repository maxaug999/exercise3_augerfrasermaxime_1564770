package decorator;

public abstract class AExtraCoverType {
	
	private float cost;
	
	public AExtraCoverType(float cost) {
		this.cost = cost;
	}
	
	public float getCoverCost() {
		return this.cost;
	}

	public void setCost(float newCost) {
		this.cost = newCost;
	}
}
