package stubs;

import decorator.AAlbum;
import decorator.IAlbumPhoto;

public class AlbumStub implements IAlbumPhoto{

	@Override
	public float calculateCost() {
		return AAlbum.ANY_DEFAULT_COST;
	}

	@Override
	public void setCost(float newCost) {
		// TODO Auto-generated method stub
		
	}

}
