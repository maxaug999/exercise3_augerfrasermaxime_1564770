package album;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import decorators.ExtraPage;
import dummies.AlbumDummy;

public class ExtraPageTests {
	
	private ExtraPage extra;
	private int extraPages = 5;
	
	@Before
	public void setUpNewAlbum() {
		extra = new ExtraPage(new AlbumDummy(), extraPages);
	}
	
	@Test
	public void extraPageIsCreated_THEN_totalCostIsReturned() {
		final float EXPECTED_PRICE = ExtraPage.INITIAL_COST * extraPages;
		assertEquals(EXPECTED_PRICE, extra.calculateCost(),0.1);
	}
	
	@Test
	public void GIVEN_anAlbum_WHEN_extraPageIsCreated_THEN_totalCostIsReturned() {
		final float EXPECTED_PRICE = ExtraPage.INITIAL_COST * extraPages;
		assertEquals(EXPECTED_PRICE, extra.calculateCost(),0.1);
	}
	
	@Test
	public void setCost_shoulResetExtraCoverCost() {
		float newCost = 5f;
		extra.setCost(newCost);
		final float EXPECTED_PRICE = newCost * extraPages ;
		assertEquals(EXPECTED_PRICE, extra.calculateCost(),0.1);
	}
}
