package album;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import decorator.AAlbum;
import decorator.IAlbumPhoto;
import decorator.IllegalFootNoteException;
import decorators.FootNote;
import dummies.AlbumDummy;
import stubs.AlbumStub;

public class FootNoteTests {
	
	private IAlbumPhoto album;
	
	@Before
	public void setUpNewAlbum() {
		album = new AlbumStub();
	}
	
	@Test
	public void footNoteIsCreated_THEN_totalCostIsReturned() {
		String footNote = "wwowowowow";
		final float EXPECTED_PRICE = FootNote.INITIAL_COST;
		assertEquals(EXPECTED_PRICE, new FootNote(new AlbumDummy(), footNote).calculateCost(),0.1);
	}

	
	@Test
	public void GIVEN_anAlbum_WHEN_footNoteIsCreated_THEN_totalCostIsReturned() {
		String footNote = "wwowowowow";
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + FootNote.INITIAL_COST;
		assertEquals(EXPECTED_PRICE, new FootNote(album,footNote).calculateCost(),0.1);
	}
	
	@Test
	public void setCost_shoulResetExtraCoverCost() {
		String footNote = "wwowowowow";
		FootNote extra = new FootNote(album, footNote);
		float newCost = 5f;
		extra.setCost(newCost);
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + newCost;
		assertEquals(EXPECTED_PRICE, extra.calculateCost(),0.1);
	}
	
	@Test (expected = IllegalFootNoteException.class)
	public void footNoteWithTooMuchCaractersShouldThrowIllegalFootNoteException() {
		String footNote = "awwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww";
		new FootNote(album, footNote);
	}
	
	@Test (expected = IllegalFootNoteException.class)
	public void footNoteWithNotEnoughCaractersShouldThrowIllegalFootNoteException() {
		String footNote = "";
		new FootNote(album, footNote);
	}
}
