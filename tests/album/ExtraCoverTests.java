package album;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import decorator.AAlbum;
import decorator.IAlbumPhoto;
import decorators.ExtraCover;
import dummies.AlbumDummy;
import dummies.ExtraCoverDummy;

public class ExtraCoverTests {
	
private IAlbumPhoto album;
	
	private ExtraCover extra;
	
	@Before
	public void setUpNewAlbum() {
		extra = new ExtraCover(new AlbumDummy(), new ExtraCoverDummy());
	}
	
	@Test
	public void extraCoverIsCreated_THEN_totalCostIsReturned() {
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + ExtraCoverDummy.ANY_COST;
		assertEquals(EXPECTED_PRICE, new ExtraCover(album, new ExtraCoverDummy()).calculateCost(),0.1);
	}
	
	@Test
	public void setCost_shoulResetExtraCoverCost() {
		ExtraCover extra = new ExtraCover(album, new ExtraCoverDummy());
		float newCost = 10f;
		extra.setCost(newCost);
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + newCost;
		assertEquals(EXPECTED_PRICE, extra.calculateCost(),0.1);
	}
}
