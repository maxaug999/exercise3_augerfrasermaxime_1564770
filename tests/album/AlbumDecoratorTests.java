package album;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import decorator.AAlbum;
import decorator.IAlbumPhoto;
import decorators.ExtraPage;
import decorators.FootNote;
import dummies.ExtraCoverDummy;
import stubs.AlbumStub;
import stubs.ExtraCoverStub;
import stubs.ExtraPageStub;
import stubs.FootNoteStub;

public class AlbumDecoratorTests {
	
	private IAlbumPhoto album;
	
	@Before
	public void setUpNewAlbum() {
		album = new AlbumStub();
	}
	
	@Test
	public void GIVEN_anAlbum_WHEN_extraCoverIsCreated_THEN_totalCostIsReturned() {
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + ExtraCoverDummy.ANY_COST;
		assertEquals(EXPECTED_PRICE, new ExtraCoverStub(album, new ExtraCoverDummy()).calculateCost(),0.1);
	}
	
	@Test
	public void GIVEN_anAlbum_WHEN_footNoteIsCreated_THEN_totalCostIsReturned() {
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + FootNote.INITIAL_COST;
		assertEquals(EXPECTED_PRICE, new FootNoteStub(album).calculateCost(),0.1);
	}
	
	@Test
	public void GIVEN_anAlbum_WHEN_extraPageIsCreated_THEN_totalCostIsReturned() {
		int newPages = 8;
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + ExtraPage.INITIAL_COST * newPages;
		assertEquals(EXPECTED_PRICE, new ExtraPageStub(album,newPages).calculateCost(),0.1);
	}
	
	@Test
	public void GIVEN_anAlbum_WHEN_footNoteANDextraCoverAreCreated_THEN_totalCostIsReturned() {
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + FootNote.INITIAL_COST + ExtraCoverDummy.ANY_COST;
		assertEquals(EXPECTED_PRICE, new ExtraCoverStub(new FootNoteStub(album),new ExtraCoverDummy()).calculateCost(),0.1);
	}
	
	@Test
	public void GIVEN_anAlbum_WHEN_footNoteANDextraCoverANDextraPagesAreCreated_THEN_totalCostIsReturned() {
		int newPages = 8;
		final float EXPECTED_PRICE = AAlbum.ANY_DEFAULT_COST + FootNote.INITIAL_COST + ExtraCoverDummy.ANY_COST + ExtraPage.INITIAL_COST * newPages;
		assertEquals(EXPECTED_PRICE, new ExtraPageStub(new ExtraCoverStub(new FootNoteStub(album),new ExtraCoverDummy()), newPages).calculateCost(),0.1);
	}
}
