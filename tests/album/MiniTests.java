package album;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import decorator.AAlbum;
import decorator.IAlbumPhoto.ColorType;

public class MiniTests {
	
	public AAlbum album;
	
	@Before
	public void setUpNewAlbum() {
		album = new Mini(ColorType.RED);
	}
	
	@Test
	public void albumShouldReturnTheColorType() {
		final ColorType EXPECTED_COLOR_TYPE = ColorType.RED;
		assertEquals(EXPECTED_COLOR_TYPE, album.getColorType());
	}
	
	@Test
	public void albumShouldCalculateCost() {
		final float EXPECTED_COST = Mini.DEFAULT_COST;
		assertEquals(EXPECTED_COST, album.calculateCost(),0.1);
	}
	
	@Test
	public void setCostShouldResetTheCost() {
		float newCost = 5f;
		album.setCost(newCost);
		final float EXPECTED_COST = newCost;
		assertEquals(EXPECTED_COST, album.calculateCost(),0.1);
	}
}
