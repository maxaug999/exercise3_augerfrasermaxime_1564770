package decorators;

import decorator.AlbumDecorator;
import decorator.IAlbumPhoto;
import decorator.IllegalFootNoteException;

public class FootNote extends AlbumDecorator{
	
	public static final float INITIAL_COST = 0.9f;
	
	private String footNote;
	private float cost = INITIAL_COST;

	public FootNote(IAlbumPhoto myComponentToDecore, String footNote) {
		super(myComponentToDecore);
		this.validateFootNote(footNote);
		this.footNote = footNote;
	}
	
	private void validateFootNote(String footNote) {
		if(footNote.length() > 80) {
			throw new IllegalFootNoteException(IllegalFootNoteException.TOO_MUCH_LETTERS);
		}
		if(footNote.length() < 1) {
			throw new IllegalFootNoteException(IllegalFootNoteException.EMPTY_NOTE);
		}
	}

	@Override
	public void setCost(float newCost) {
		this.cost = newCost;
	}

	@Override
	public float getCost() {
		return cost;
	}
}
