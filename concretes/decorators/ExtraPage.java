package decorators;

import decorator.AlbumDecorator;
import decorator.IAlbumPhoto;

public class ExtraPage extends AlbumDecorator{
	
	public static final float INITIAL_COST = 3.0f;
	
	private int numberOfExtraPages;
	private float cost = INITIAL_COST;
	
	public ExtraPage(IAlbumPhoto myComponentToDecore, int numberOfExtraPages) {
		super(myComponentToDecore);
		this.numberOfExtraPages = numberOfExtraPages;
	}
	
	@Override
	public void setCost(float newCost) {
		this.cost = newCost;
	}
	
	@Override
	public float getCost() {
		return cost * numberOfExtraPages;
	}
}
