package decorators;

import decorator.AExtraCoverType;

public class SwedenExtraCover extends AExtraCoverType{
	
	public static final float INITIAL_UNIT_COST = 4f;
	
	public SwedenExtraCover(float coverCost)
    {
		super(INITIAL_UNIT_COST);
    }
}
