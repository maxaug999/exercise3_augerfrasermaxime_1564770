package decorators;

import decorator.AExtraCoverType;
import decorator.AlbumDecorator;
import decorator.IAlbumPhoto;

public class ExtraCover extends AlbumDecorator{
	
	private AExtraCoverType extraCoverType;
	
	public ExtraCover(IAlbumPhoto myComponentToDecore, AExtraCoverType extraCoverType) {
		super(myComponentToDecore);
		this.extraCoverType = extraCoverType;
	}
	
	public void setCost(float newCost) {
		this.extraCoverType.setCost(newCost);
	}
	
	@Override
	public float getCost() {
		return this.extraCoverType.getCoverCost();
	}
	
}
