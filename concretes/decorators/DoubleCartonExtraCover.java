package decorators;

import decorator.AExtraCoverType;

public class DoubleCartonExtraCover extends AExtraCoverType{
	
	public static final float INITIAL_UNIT_COST = 3f;
	
	public DoubleCartonExtraCover()
    {
		super(INITIAL_UNIT_COST);
    }
}
