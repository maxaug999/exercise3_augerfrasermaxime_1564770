package decorators;

import decorator.AExtraCoverType;

public class LeatherExtraCover extends AExtraCoverType{
	
	public static final float INITIAL_UNIT_COST = 5f;
	
	public LeatherExtraCover(float coverCost)
    {
         super(INITIAL_UNIT_COST);
    }
}
