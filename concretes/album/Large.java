package album;

import decorator.AAlbum;

public class Large extends AAlbum{
	public static final float DEFAULT_COST = 24.99f;
	public static final FormatType DEFAULT_FORMAT= FormatType.LEGAL;
	public static final OrientationType DEFAULT_ORIENTATION = OrientationType.PORTRAIT;
	
	public Large(ColorType colorType) {
		super(colorType,DEFAULT_COST,DEFAULT_FORMAT,DEFAULT_ORIENTATION);
	}
}
