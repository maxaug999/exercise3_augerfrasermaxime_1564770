package album;

import decorator.AAlbum;

public class Standard extends AAlbum{
	public static final float DEFAULT_COST = 19.99f;
	public static final FormatType DEFAULT_FORMAT= FormatType.LETTER_US;
	public static final OrientationType DEFAULT_ORIENTATION = OrientationType.PORTRAIT;
	
	public Standard(ColorType colorType) {
		super(colorType,DEFAULT_COST,DEFAULT_FORMAT,DEFAULT_ORIENTATION);
	}
}
