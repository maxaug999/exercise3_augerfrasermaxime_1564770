package album;

import decorator.AAlbum;

public class Mini extends AAlbum{
	public static final float DEFAULT_COST = 9.99f;
	public static final FormatType DEFAULT_FORMAT= FormatType.A5;
	public static final OrientationType DEFAULT_ORIENTATION = OrientationType.LANDSCAPE;
	
	public Mini(ColorType colorType) {
		super(colorType,DEFAULT_COST,DEFAULT_FORMAT,DEFAULT_ORIENTATION);
	}
}
