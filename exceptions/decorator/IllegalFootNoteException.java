package decorator;

@SuppressWarnings("serial")
public class IllegalFootNoteException extends IllegalArgumentException{
	
	public static final String TOO_MUCH_LETTERS = "Trop de caract�res";
	public static final String EMPTY_NOTE = "Ajouter au moins 1 caract�re";
	
	public IllegalFootNoteException(String e) {
		super(e);
	}
}
